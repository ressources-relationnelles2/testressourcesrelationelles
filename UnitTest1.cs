using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright;
using Microsoft.Playwright.MSTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Net.Http;
using System.Threading.Tasks;



namespace PlaywrightTests;

[TestClass]
public class UnitTest1 : PageTest
{
    [TestMethod]
    public async Task Connexion()
    {

        await Page.GotoAsync("http://localhost:5173/");

        await Page.GetByRole(AriaRole.Link, new() { Name = "Me connecter" }).ClickAsync();

        await Page.GetByPlaceholder("Email address").ClickAsync();

        await Page.GetByPlaceholder("Email address").FillAsync("test@test.fr");

        await Page.GetByPlaceholder("Enter your password").ClickAsync();

        await Page.GetByPlaceholder("Enter your password").FillAsync("test");
            
        await Page.GetByRole(AriaRole.Button, new() { Name = "Log In" }).ClickAsync();


    }
}








       