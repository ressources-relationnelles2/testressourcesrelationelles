using Microsoft.Playwright;
using Microsoft.Playwright.MSTest;


namespace PlaywrightTests;

[TestClass]
public class ExporterStats : PageTest
{
    [TestMethod]
    public async Task ExporterStatsTest()
    {

        await Page.GotoAsync("http://localhost:5173/");


        await Page.GetByRole(AriaRole.Link, new() { Name = "Administration" }).ClickAsync();

        var download = await Page.RunAndWaitForDownloadAsync(async () =>
        {
            await Page.GetByRole(AriaRole.Button, new() { Name = "Exporter les statistiques" }).ClickAsync();
        });

    }
}

