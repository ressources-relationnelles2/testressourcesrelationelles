using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright;
using Microsoft.Playwright.MSTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Net.Http;
using System.Threading.Tasks;



namespace PlaywrightTests;

[TestClass]
public class DefilerLesRessources : PageTest
{


    [TestMethod]

    public async Task DefilerLesRessourcesTest()
    {
         await Page.GotoAsync("http://localhost:5173/");


        await Page.GetByRole(AriaRole.Link, new() { Name = "Catalogue de ressource" }).ClickAsync();

        await Page.GetByText("Titre 1Description de la ressource 1").ClickAsync();

        await Page.GetByRole(AriaRole.Button, new() { Name = "Fermer" }).ClickAsync();

        await Page.GetByText("Titre 2Description de la ressource 2").ClickAsync();

        await Page.GetByRole(AriaRole.Button, new() { Name = "Fermer" }).ClickAsync();

        await Page.GetByText("Titre 3Description de la ressource 3").ClickAsync();

        await Page.GetByRole(AriaRole.Button, new() { Name = "Fermer" }).ClickAsync();

        await Page.GetByRole(AriaRole.Link, new() { Name = "Tableau de bord" }).ClickAsync();

        await Page.GetByText("Titre de la ressource favorite 1Description de la ressource favorite 1").ClickAsync();

        await Page.GetByRole(AriaRole.Button, new() { Name = "Fermer" }).ClickAsync();

        await Page.GetByText("Titre de la ressource suivie 1Description de la ressource suivie 1").ClickAsync();

        await Page.GetByRole(AriaRole.Button, new() { Name = "Fermer" }).ClickAsync();

        await Page.GetByText("Description de la ressource exploitee 1").ClickAsync();

        await Page.GetByRole(AriaRole.Button, new() { Name = "Fermer" }).ClickAsync();

    }
}

